---
layout: "base"
permalink: "/"
---
<svg style="display: none" version="2.0">
  <defs>
    <symbol id="svg-prev" viewBox="0 0 24 24" width="40" height="40" class="bordered">
      <circle cx="12" cy="12" r="10" />
      <line x1="8" y1="12" x2="16" y2="12" />
      <polyline points="12 8, 8 12, 12 16" />
    </symbol>
    <symbol id="svg-next" viewBox="0 0 24 24" width="40" height="40" class="bordered">
      <circle cx="12" cy="12" r="10" />
      <line x1="8" y1="12" x2="16" y2="12" />
      <polyline points="12 8, 16 12, 12 16" />
    </symbol>
  </defs>
</svg>

<section>
<div>
<img class="profile" src="/img/vnourdin.png" width="160" alt="Ma photo de profil" loading="eager">
<h1 style="margin-bottom: 0">Valentin Nourdin</h1>
<h2>Développeur Web</h2>

Bonjour, je m'appelle Valentin Nourdin et je suis Développeur Web indépendant !

Fort de [mon expérience](https://fr.linkedin.com/in/vnourdin) en tant que **Data Engineer** puis **Data Ops** et très attiré par les possibilités du web, j'ai créé mon entreprise en 2021 afin de combiner mes compétences en Data et en développement et ainsi répondre au mieux à vos projets.

Je m'occupe de toute la partie techniques des projets, je gère des serveurs hébergés en France, développe le back et le front-end et j'ai une bonne expérience en CI/CD et plus généralement en automatisation.

Mon travail se concentre sur deux axes : **la sobriété énergétique** et **l'accessibilité** !
</div>
</section>

<section data-bg="/img/save-energy-g209029847_1920.png">
<div class="glider-container">

## Sobriété énergétique

  <div class="glider-contain">
    <div class="glider">
      <div>

Je réduit au maximum le poids de mes sites et applications afin de minimiser leur impact énergétique.
Pour y arriver, je propose des **sites statiques** accompagnés de **fonctions à la demande**, pour les interactions avec les utilisateurs.

Le tout est hébergé sur des serveurs français, afin d'être au plus près des visiteurs et ainsi réduire la consommation énergétique du réseau.

De plus, ces optimisations **améliorent les performances** de mes projets, ce qui permet une meilleure **rétention des utilisateurs** et **un bon référencement**.

  </div>
  <div>

Je n'utilise pas de serveur back-end, comme pour des sites dynamiques, ce qui me permet de travailler avec **un seul langage** et d'héberger plus de sites sur la même machine et ainsi **réduire les coûts**.

En thermes de technologies, je travailles avec **Javascript**, NodeJS, **[Eleventy [EN]](https://www.11ty.dev/)**, **[OpenFaas [EN]](https://www.openfaas.com/)**, **[GitLab [EN]](https://about.gitlab.com/)**, Terraform.

Mes projets sont hébergés chez **[OVH](https://www.ovhcloud.com/fr/) Strasbourg** et mes bases de données ainsi que le stockage de médias sont chez **[AWS](https://aws.amazon.com/fr/) Paris**.

  </div>
</div>

<button aria-label="Précédent" class="glider-prev">«</button>
<button aria-label="Suivant" class="glider-next">»</button>
<div role="tablist" class="glider-dots"></div>
</div>

</div>
</section>

<section>
<div>

## Accessibilité

Une grande partie du web est aujourd'hui compliquée d'utilisation pour de très nombreuses personnes, par **manque d'accessibilité**. Cela concerne les personnes en situation de handicap, les personnes dyslexiques ou dyspraxique, les personnes ayant des troubles de l'attention ou encore les personnes sujettes à l’[illectronisme](https://fr.wikipedia.org/wiki/Illectronisme).

Étant sensibilisé à ce sujet et concerné par certains aspects, je m'implique afin de **proposer des solutions techniques adaptées** et **accompagner mes clients** dans la rédaction de contenus et designs plus accessibles.

</div>
</section>

<section data-bg="/img/dsc3631web.jpg">
<div class="glider-container">

## Réalisations
  <div class="glider-contain">
    <div class="glider">
      <div>

### [Défis-Rameur](https://www.defis-rameur.com/)

![Page d'accueil du site www.defis-rameur.com, on y vois quelqu'un ramer.](/img/Screen-defis.png)

Le premier projet d'envergure que j'ai réalisé est le site [www.defis-rameur.com](https://www.defis-rameur.com/) qui permet de **participer à des courses en ligne scénarisées sur rameur**.

La première course s'est déroulée en novembre 2021, sur 18 jours, en parallèle de la Transat Jacques Vabre à la voile, et une seconde épreuve aura lieu **du 5 au 23 septembres 2022**.

  </div>
  <div>

### [Défis-Rameur](https://www.defis-rameur.com/)

![Exemple de graphique présentant des résultats de course sur www.defis-rameur.com](/img/Screen-graph.png)

Ce site propose des fonctionnalités comme l'**inscription**, la **connexion** à un espace personnel, la **récupération périodique automatisée** des données de course de toutes les équipes, le calcul des résultats et l'affichage des classements ainsi que des **statistiques détaillées** pour chaque équipe.

Vous pouvez également suivre le 600Cal Challenge, qui est ouvert sans limite de temps sur [600cal.defis-rameur.com](https://600cal.defis-rameur.com/).

  </div>
  <div>

### [Creations-Marietournelle](https://www.creations-marietournelle.fr/)

![Page d'accueil du site www.creations-marietournelle.fr, on y vois des confitures.](/img/Screen-marietournelle.png)

Ce site vitrine réalisé pour une artisane confiturière permet de présenter son travail et ses créations tout en restant sobre.

Mon travail s'est porté sur le choix des couleurs thématiques qui soit contrastées pour des personnes daltoniennes, ainsi que sur le design.\
Ce dernier est modulable tout en laissant la gestion du contenu à la cliente.

  </div>

</div>

<button aria-label="Précédent" class="glider-prev">«</button>
<button aria-label="Suivant" class="glider-next">»</button>
<div role="tablist" class="glider-dots"></div>
</div>

</div>
</section>

<section>
<div>

# Intéressé·e ?

Si vous avez un projet à me confier, n'hésitez pas à me contacter par mail à [contact@vnourdin.dev](mailto:contact@vnourdin.dev), nous pourrons l'affiner ensemble avant de faire un devis.

Vous pouvez également contacter William Laîné à [contact@coach-rameur.com](mailto:contact@coach-rameur.com), qui pourra vous faire un retour sur mon travail sur Défis-Rameur.

#

<footer></footer>

# À bientôt 👋
</div>
</section>
