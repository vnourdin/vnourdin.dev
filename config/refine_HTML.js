const Image = require("@11ty/eleventy-img");
const cheerio = require("cheerio");

const htmlMinifier = require("html-minifier-terser");

module.exports = function(eleventy, pluginOptions = {}) {

  function makeExternalLinksSafe($) {
    $("a")
      .each(function () {
        if ($(this).attr("href")?.startsWith("http")) {
          $(this).attr("rel", "noopener");
          $(this).attr("target", "_blank");
        }
      });
  }

  function optimizeImages($) {
    $("img")
      .each(function () {
        const imgAlt = $(this).attr("alt") || "";
        const imgTitle = $(this).attr("title") || "";
        const imgWidth = $(this).attr("width");
        const imgLoad = $(this).attr("loading");
        const imgSrc = pluginOptions.inputDir + $(this).attr("src");
        const imgClasses = $(this).attr("class") || "";

        let widths = [360, 480, 750, 1000, 1250, 1500, 1750];

        if (imgWidth) {
          widths = widths.filter(size => size <= imgWidth);
          if (!widths.length)
            widths = [360];
        }

        const options = {
          widths: widths,
          formats: ["webp", "jpeg"],
          outputDir: `${pluginOptions.outputDir}/img/`,
          sharpWebpOptions: {
            quality: imgClasses.includes("is-full-width") ? 90 : 70,
          },
          sharpJpegOptions: {
            quality: 70,
          },
        };

        Image(imgSrc, options);

        const metadata = Image.statsSync(imgSrc, options);

        const lowsrc = metadata.jpeg[0];
        const highsrc = metadata.jpeg[metadata.jpeg.length - 1];

        const sources = Object.values(metadata)
          .map(imageFormat => {
            const type = imageFormat[0].sourceType;
            const srcSet = imageFormat.map(entry => entry.srcset).join(", ");
            return `  <source type="${type}" srcset="${srcSet}" sizes="100vw">`;
          }).join("\n");

        const loading = (!imgLoad || imgLoad == "lazy") ? "loading=\"lazy\" decoding=\"async\"" : `loading="${imgLoad}"`;

        const ratio = imgWidth ? imgWidth / highsrc.width : 1;

        const newElem = `
          <picture>
            ${sources}
            <img
              src="${lowsrc.url}"
              alt="${imgAlt}"
              title="${imgTitle}"
              class="${imgClasses}"
              width="${highsrc.width * ratio}"
              height="${highsrc.height * ratio}"
              ${loading} >
          </picture>`;

        const parent = $(this).parent();
        if (parent.children().length === 1)
          parent.replaceWith(newElem);
        else
          $(this).replaceWith(newElem);
      });

    $("[data-bg]")
      .each(function () {
        const bgSrc = pluginOptions.inputDir + $(this).attr("data-bg");

        const options = {
          widths: [1500],
          formats: ["webp", "jpeg"],
          outputDir: `${pluginOptions.outputDir}/img/`,
          sharpWebpOptions: {
            quality: 70,
          },
          sharpJpegOptions: {
            quality: 70,
          },
        };

        Image(bgSrc, options);

        const metadata = Image.statsSync(bgSrc, options);

        $(this).attr("data-bg", metadata.webp[metadata.webp.length - 1].url);
        $(this).attr("data-bgjpeg", metadata.jpeg[metadata.jpeg.length - 1].url);
      });
  }

  eleventy.addTransform("refine_HTML", function(content) {
    if((this.outputPath && this.outputPath.endsWith(".html"))) {

      const $ = cheerio.load(content);

      makeExternalLinksSafe($);

      optimizeImages($);

      return htmlMinifier.minify($.html(), {
        collapseBooleanAttributes: true,
        collapseWhitespace: true,
        decodeEntities: true,
        html5: true,
        minifyCSS: true,
        minifyJS: true,
        removeComments: true,
        removeEmptyAttributes: true,
        sortAttributes: true,
        sortClassName: true,
        useShortDoctype: true,
      });
    }

    return content;
  });
};
