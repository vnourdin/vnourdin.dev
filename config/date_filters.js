module.exports = eleventy => {
  eleventy.addNunjucksGlobal("date",
    (tz, date) => {
      const utcDate = date ? new Date(date) : new Date();
      return new Date(utcDate.toLocaleString("en-US", {timeZone: tz ?? "Europe/Paris"}) + " UTC");
    });

  eleventy.addNunjucksFilter("dateStr",
    date => {
      return date.toLocaleDateString("fr-FR");
    });

  eleventy.addNunjucksFilter("timeStr",
    date => {
      return date.toLocaleTimeString("fr-FR");
    });
};
