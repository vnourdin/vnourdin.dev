const { stat, mkdir, readdir, readFile, writeFile } = require("node:fs/promises");
const crypto = require("node:crypto");

// CSS dependencies
const postcss = require("postcss");
const postcssCombineDuplicatedSelectors = require("postcss-combine-duplicated-selectors");
const autoprefixer = require("autoprefixer");
const postcssDiscardComments = require("postcss-discard-comments");
const cssnano = require("cssnano");
// CSS purge dependencies
const purgecss = require("@fullhuman/postcss-purgecss");
const purgecssHtml = require("purgecss-from-html");

// JS dependencies
const { minify } = require("terser");
let fileMap;

( async () => {
  fileMap = await readFile("_hashed-filemap.json", { encoding: "utf8" })
    .then(data => JSON.parse(data))
    .catch(() => {});
} )();

async function hashJSFile(file, pluginOptions) {
  let content = await readFile(`${pluginOptions.inputDir}/assets/js/${file}`, { encoding: "utf8" });

  content = content.replaceAll(/\{\{ getHashedFileName\(([^)]*)\) \}\}/g, (_, match) => fileMap[match]);

  const options = {
    compress: {
      unsafe: true,
    },
  };

  const minified = await minify(content, options);
  const hash = crypto.createHash("sha1").update(minified.code).digest("hex").substring(0, 10);
  const fileNameSplit = file.split(".");
  const hashedFileName = `${fileNameSplit[0]}-${hash}.${fileNameSplit[1]}`;
  fileMap = {
    ...fileMap,
    [`/js/${file}`]: `/js/${hashedFileName}`
  };

  await writeFile(`${pluginOptions.outputDir}/js/${hashedFileName}`, minified.code);
}

module.exports = function(eleventy, pluginOptions = {}) {
  eleventy.on("eleventy.before", async () => {
    // CSS files
    console.log("Minifying and hashing CSS...");

    await stat(`${pluginOptions.outputDir}/css`).catch(() => mkdir(`${pluginOptions.outputDir}/css`, { recursive: true }));

    const postCssEngine = postcss([
      postcssCombineDuplicatedSelectors({removeDuplicatedProperties: true}),
      autoprefixer,
      postcssDiscardComments({removeAll: true}),
      cssnano,
    ]);

    for (const cssName of ["glider"]) {
      const path = `${pluginOptions.inputDir}/assets/css/${cssName}.css`;
      const file = await readFile(path);
      const css = postcss.parse(file, { from: path });

      await postCssEngine.process(css, {from: undefined})
        .then(result => {
          result.warnings().forEach(warn => {console.warn(warn.toString());});
          const hash = crypto.createHash("sha1").update(result.css).digest("hex").substring(0, 10);
          const hashedFileName = `${cssName}-${hash}.css`;
          fileMap = {
            ...fileMap,
            [`/css/${cssName}.css`]: `/css/${hashedFileName}`
          };
          return writeFile(`${pluginOptions.outputDir}/css/${hashedFileName}`, result.css);
        });
    }

    const inputChotaPath = `${pluginOptions.inputDir}/assets/css/chota.css`;
    const inputCustomPath = `${pluginOptions.inputDir}/assets/css/custom.css`;

    const chota = await readFile(inputChotaPath);
    const custom = await readFile(inputCustomPath);

    const chotaRoot = postcss.parse(chota, { from: inputChotaPath });
    const customRoot = postcss.parse(custom, { from: inputCustomPath });
    const styleCss = chotaRoot.append(customRoot);

    await postCssEngine.process(styleCss, {from: undefined})
      .then(result => {
        result.warnings().forEach(warn => {console.warn(warn.toString());});
        const hash = crypto.createHash("sha1").update(result.css).digest("hex").substring(0, 10);
        const hashedFileName = `style-${hash}.css`;
        fileMap = {
          ...fileMap,
          "/css/style.css": `/css/${hashedFileName}`
        };
        return writeFile(`${pluginOptions.outputDir}/css/${hashedFileName}`, result.css);
      });

    // JS files
    console.log("Minifying and hashing JS...");

    await stat(`${pluginOptions.outputDir}/js`).catch(() => mkdir(`${pluginOptions.outputDir}/js`, { recursive: true }));

    for (const file of await readdir(`${pluginOptions.inputDir}/assets/js`)) {
      await hashJSFile(file, pluginOptions);
    }

    // Save filemap
    await writeFile("_hashed-filemap.json", JSON.stringify(fileMap));
  });

  eleventy.on("eleventy.after", async () => {
    console.log("Purging CSS...");

    const cssPath = `${pluginOptions.outputDir}/${fileMap["/css/style.css"]}`;

    await readFile(cssPath)
      .then(css => {
        return postcss([
          purgecss({
            content: [`${pluginOptions.outputDir}/**/*.html`],
            extractors: [{extractor: purgecssHtml, extensions: ["html"]}],
            safelist: ["class"],
            fontFace: true
          }),
        ])
          .process(css, { from: cssPath, to: cssPath })
          .then(result => {
            result.warnings().forEach(warn => {console.warn(warn.toString());});
            return writeFile(cssPath, result.css);
          });
      })
      .catch(err => console.log(`[WARN] Failed to optimize CSS: ${err.message}`));
  });

};
