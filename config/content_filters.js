const markdownIt = require("markdown-it");

module.exports = eleventy => {
  const markdown = markdownIt({
    html: true,
    breaks: true,
    linkify: true,
    typographer: true,
  });

  eleventy.addNunjucksFilter("mdToHtml", rawString => markdown.render(rawString));

  eleventy.addNunjucksFilter("youtubeToLite", content => {
    if (!content)
      return "";

    const pattern = /https:\/\/w{3}\.youtube\.com\/watch\?v=([A-Za-z0-9-_]{11})/g;

    const matches = [...content.matchAll(pattern)];

    if (matches.length)
      content = "<script type=\"module\" src=\"/js/lite-youtube.js\"></script><script>var link = document.createElement('link');link.rel=\"stylesheet\";link.type=\"text/css\";link.href=\"/css/lite-youtube.css\";document.head.appendChild(link);</script>" + content;

    matches.forEach(match => {
      const link = match[0];
      const id = match[1];
      content = content.replace(link, `<div class="video"><lite-youtube videoID="${id}" style="background-image: url('https://i.ytimg.com/vi/${id}/hqdefault.jpg');"><div class="lty-playbtn"><span class="lyt-visually-hidden">Lire</span></div></lite-youtube></div>`);
    });

    return content;
  });

  eleventy.addNunjucksFilter("vimeoToLite", content => {
    if (!content)
      return "";

    const pattern = /vimeo\(([A-Za-z0-9-_]{9})\)/g;

    const matches = [...content.matchAll(pattern)];

    if (matches.length)
      content = "<script type=\"module\" src=\"/js/lite-vimeo.js\"></script>" + content;

    matches.forEach(match => {
      const link = match[0];
      const id = match[1];
      content = content.replace(link, `<div class="video"><lite-vimeo videoid="${id}"></lite-vimeo></div>`);
    });

    return content;
  });
};
