const contentFilters = require("./config/content_filters.js");
const dateFilters = require("./config/date_filters.js");
const refineHTML = require("./config/refine_HTML.js");
const minifyAssets = require("./config/minify_assets.js");

const inputDir = "src";
const outputDir = "public";

let hashFileMap;
const getHashedFileName = fileName => {
  if (!hashFileMap)
    hashFileMap = require("./_hashed-filemap.json");
  return hashFileMap[fileName];
};

module.exports = function(eleventy) {

  //// Plugins
  eleventy.addPlugin(contentFilters);
  eleventy.addPlugin(dateFilters);
  eleventy.addPlugin(refineHTML, {inputDir: inputDir, outputDir: outputDir});
  eleventy.addPlugin(minifyAssets, {inputDir: inputDir, outputDir: outputDir});

  // Filters

  eleventy.addNunjucksFilter("getHashedFileName", getHashedFileName);

  //// Eleventy config

  eleventy.setDataDeepMerge(true);
  eleventy.setTemplateFormats("md, njk, js");

  eleventy.addPassthroughCopy(`${inputDir}/*.ico`);
  eleventy.addPassthroughCopy(`${inputDir}/*.png`);
  eleventy.addPassthroughCopy(`${inputDir}/fonts/`);
  // to generate fonts, bash> glyphhanger --subset=./*.ttf --LATIN --formats=woff2,woff,ttf

  eleventy.addWatchTarget(`${inputDir}/css/*.css`);

  return {
    markdownTemplateEngine: "njk",
    htmlTemplateEngine: false,
    dataTemplateEngine: false,

    dir: {
      input: inputDir,
      output: outputDir
    }
  };
};
