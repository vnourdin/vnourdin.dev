module.exports = {
  env: {
    "browser": true,
    "node": true,
    "commonjs": true,
    "es2021": true,
  },
  extends: "eslint:recommended",
  rules: {
    "indent": ["error", 2],
    "linebreak-style": ["error", "unix"],
    "quotes": ["error", "double"],
    "semi": ["error", "always"],
    "no-empty": "error",
    "no-cond-assign": ["error", "always"],
    "camelcase": ["error"]
  },
  overrides: [
    {
      "files": ["*.mjs"],
      "parserOptions": {
        "sourceType": "module",
      },
    },
  ],
  parserOptions: {
    "ecmaVersion": 12,
  },
};
